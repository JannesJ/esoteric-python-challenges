# Esoteric Python Challenges

Esoteric programming challenges for the Python programming language.

These challenges are maintained by the Python Discord server. [Come check us out!](https://pythondiscord.com/)

# Challenge Information

Every week or so, we will be posting a new challenge to solve.
Generally, challenges will involve a problem to solve (e.g. implement a specific algorithm), a goal (e.g. code golf (shortest number of characters in source code)) and sometimes even some
restrictions (e.g. no use of imports)

Please note that while these challenges have these rules and restrictions, you are free to submit solutions that do *not* take them into consideration. If you do, please simply make note
of the limitations of your solutions in a comment in the source code! The ultimate goal is to have **fun**, be creative and show off your esoteric coding chops! There are no prizes or true
winners. You win if you had fun trying to solve some challenges or learn a thing or two from other peoples' solutions :) Feel free to golf your solutions to non-golf challenges or to add further 
restrictions for your own challenge.

# Submission Guidelines

At the top of the file, add a comment block with your name, and any notes regarding your solution (e.g. restrictions not followed, instructions on how to use the code, etc.)

> Tip: If your submission is a codegolf, it'd be helpful to fellow coders if you also appended to your code a less-condensed version of the golf annotated with comments on how it works

You may submit more than one solution for a challenge! Just append subsequent solutions to the same file.

## Option A: Create a merge request

* Clone the repository locally
* In the relevant challenge directory, add your solution as a `.py` file named as `your_name.py`, `your_name` being whatever handle you wish to be referred to as.
* Submit a merge request
* (Optionally, ping Shawn on Discord if your MR goes unnoticed for a little while!)

## Option B: Get Shawn to add it

If you're not comfortable using Git or simply don't want to bother cloning the repository, you can submit your code as a snippet in the Discord chat (`#esoteric-python`), tagging your post with
the challenge number and pinging Shawn and I'll get around to put it up for you as soon as I am able :)

## Licensing Information

All solutions submitted to this repository will be licensed and freely available under the MIT License. By submitting an entry, you acknowledge that your code may be used by others.